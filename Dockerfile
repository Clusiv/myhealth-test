FROM golang:1.12.5 as builder

ENV GO111MODULE=on
WORKDIR $GOPATH/src/go-app/
COPY . .

RUN go mod download
RUN CGO_ENABLED=0 go build -a -installsuffix cgo -o /go-app . && \
    chmod 755 /go-app

FROM scratch

COPY --from=builder /go-app /go-app
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

EXPOSE 9090

CMD ["/go-app"]